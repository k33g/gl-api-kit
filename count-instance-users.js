const GitLab = require("./libs/gl-lib");

async function fetchUsers({perPage, page}) {
  return GitLab.fetchUsers({perPage, page})
}

async function getInstanceUsers() {
  try {
    var stop = false, page = 1, users = []

    while (!stop) {
      await fetchUsers({perPage:10, page:page}).then(someUsers => {
        if(someUsers.length == 0) { stop = true }
        users = users.concat(someUsers.map(user => {
          return { 
              username: user.username
            , web_url: user.web_url
            , state: user.state
            , email: user.email 
            , is_admin: user.is_admin
          }
        }))
      })
      page +=1
    }
    return users
  } catch (error) {
    console.log("😡:", error)
  }
}

async function batch() {
  return await getInstanceUsers()
}

batch().then(results => {
  
  let activeUsers = results.filter(user => user.state == "active")
  let blockedUsers = results.filter(user => user.state == "blocked")
  let activeAdmins = activeUsers.filter(user => user.is_admin)
  
  console.log(`active users (including ${activeAdmins.length} admins): ${activeUsers.length}`)
  console.log(`blocked users: ${blockedUsers.length}`)
  
})

/*

{ id: 1,
  name: 'Administrator',
  username: 'root',
  state: 'active',
  avatar_url:
   'https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon',
  web_url: 'http://gitlab-faas.test/root',
  created_at: '2018-08-15T10:32:22.899Z',
  bio: null,
  location: null,
  skype: '',
  linkedin: '',
  twitter: '',
  website_url: '',
  organization: null,
  last_sign_in_at: '2018-08-15T10:35:39.527Z',
  confirmed_at: '2018-08-15T10:32:22.754Z',
  last_activity_on: null,
  email: 'admin@example.com',
  theme_id: 1,
  color_scheme_id: 1,
  projects_limit: 100000,
  current_sign_in_at: '2018-08-15T10:35:39.527Z',
  identities: [],
  can_create_group: true,
  can_create_project: true,
  two_factor_enabled: false,
  external: false,
  private_profile: null,
  is_admin: true,
  shared_runners_minutes_limit: null }

*/