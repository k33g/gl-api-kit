const GLClient = require("./gl-cli").GLClient;
const tools = require("./gl-cli").tools;
const config = require("./config").config;

let glClient = new GLClient({
  baseUri: `${config.url}/api/v4`,
  token: config.token
});

/* ===== Users ===== */
function fetchUserByHandle({handle}) {
  return glClient
    .get({
      path: `/users?username=${handle}`
    })
    .then(response => {
      return response.data[0];
    })
    .catch(error => {
      return error;
    });

}

function fetchUsers({perPage, page}) {
  return glClient   // return a promise
    .get({path: `/users?per_page=${perPage}&page=${page}`})
    .then(response => response.data)
    .catch(error => error)
}


/* ===== Groups ===== */
/*
POST /groups
https://docs.gitlab.com/ee/api/groups.html#new-group

name	string	yes	The name of the group
path	string	yes	The path of the group
description	string	no	The group's description
visibility	string	no	The group's visibility. Can be private, internal, or public.

Add avatar to group: https://gitlab.com/gitlab-org/gitlab-ce/issues/26212

*/

function createGroup({groupName, groupPath, description, visibility}) {
  // return a promise
  return glClient
    .post({
      path: `/groups`,
      data: {
        name: groupName,
        path: groupPath,
        description: description,
        visibility: visibility
      }
    })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return error;
    });
}

/* --- add member to a group ---
  POST /groups/:id/members
  https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project

id	integer/string	yes	The ID or URL-encoded path of the project or group owned by the authenticated user
user_id	integer	yes	The user ID of the new member
access_level	integer	yes	A valid access level
expires_at	string	no	A date string in the format YEAR-MONTH-DAY

access_level:

10 => Guest access
20 => Reporter access
30 => Developer access
40 => Maintainer access
50 => Owner access # Only valid for groups


TODO: write addMemberByHandleToGroup
*/
function addMemberByIdToGroup({groupName, userId, accessLevel}) {
  return glClient
    .post({
      path: `/groups/${tools.urlEncodedPath({name:groupName})}/members`,
      data: {
        user_id: userId,
        access_level: accessLevel
      }
    })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return error;
    });
}


/* --- add member to a project ---
  TODO: 👋
  POST /projects/:id/members
  https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
*/

/* ===== Projects ===== */

function createProjectInGroup({groupName, groupPath, projectName}) {

//let groupName = "account-management";
//let groupPath = "gitlab-com/account-management";
//let projectName = "dwp";

  // 1- get the id of the group
  glClient
    .get({
      path: `/namespaces?search=${tools.urlEncodedPath({
        name: groupName
      })}`
    })
    .then(response => {

      if (
        response.data[0].name == groupName &&
        response.data[0].full_path == groupPath
      ) {
        let namespaceId = response.data[0].id;
        // 2- create the project
        glClient
          .post({
            path: `/projects`,
            data: {
              name: projectName,
              namespace_id: namespaceId
            }
          })
          .then(response => {
            return response.data;
          })
          .catch(error => {
            return error;
          });
      }
      return response.data;
    })
    .catch(error => {
      return error;
    })

}

/* ===== Files ===== */

function createFile({
  projectName,
  nameSpace,
  filePath,
  branch,
  content,
  commitMessage
}) {
  let id = tools.urlEncodedPath({name: `${nameSpace}%2F${projectName}`}) 
  let path = tools.urlEncodedPath({name: filePath})
  let commit_message = tools.urlEncodedPath({name: commitMessage})
  let branch_name = tools.urlEncodedPath({name: branch})

  return glClient
    .post({
      path: `/projects/${id}/repository/files/${path}?branch=${branch_name}&commit_message=${commit_message}`,
      data: {
        content: tools.formatText(content)
      }
    })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log(error)
      return error;
    });
}


/* ===== CI/CD ===== */

/* --- Create Group Variables --- 
  https://docs.gitlab.com/ee/api/group_level_variables.html#create-variable

POST /groups/:id/variables


id	integer/string	yes	The ID of a group or URL-encoded path of the group owned by the authenticated user
key	string	yes	The key of a variable; must have no more than 255 characters; only A-Z, a-z, 0-9, and _ are allowed
value	string	yes	The value of a variable
protected	boolean	no	Whether the variable is protected

*/

function createGroupVariable({groupName, variable, value, protected}) {
  return glClient
    .post({
      path: `/groups/${tools.urlEncodedPath({name:groupName})}/variables`,
      data: {
          key: variable
        , value: value
        , protected: protected
      }
    })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return error;
    });
}

/* ===== Labels ===== */

function createLabel({ name, color, description, userOrGroup, project }) {
  return glClient.post({
    path: `/projects/${tools.urlEncodedPath({name: `${userOrGroup}%2F${project}`})}/labels`,
    data: { name, color, description }
  })
  .then(response => {
    return response.data;
  })
  .catch(error => {
    return error;
  });
}

function createGroupLabel({ name, color, description, userOrGroup }) {
  //TODO
}


/* ===== Issues ===== */


module.exports = {
    createGroup: createGroup
  , fetchUserByHandle: fetchUserByHandle
  , addMemberByIdToGroup: addMemberByIdToGroup
  , createGroupVariable: createGroupVariable
  , createProjectInGroup: createProjectInGroup
  , createFile: createFile
  , createLabel: createLabel
  , fetchUsers: fetchUsers

};