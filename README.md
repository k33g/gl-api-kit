# gl-api-kit

> aka GitLab API recipes 🦊

> This is a WIP :construction:

> :warning: use at your own risks (do a backup, test in on a test instance, etc...)

## Run a recipe

```shell
TOKEN="YOUR_GITLAB_TOKEN" GITLAB_URL="http://YOUR_GITLAB_INSTANCE.URL" node script_name
```

## Recipes

### `count-instance-users.js`

Get all the users of an instance, and count the active users, the administrators and the blocked users

### `create-group-add-users.js`

Create a group and add users to this group.

The data are described in a json object:

```json
let demo = {
    group: {
        name: "death-star"
      , path: "death-star"
      , description: "Death Star Project"
      , visibility: "public"
    }
  , users: ["k33g", "babs", "buster"]
}
```
