const GitLab = require("./libs/gl-lib");

async function createGroup({groupName, groupPath, description, visibility}) {
  return GitLab.createGroup({groupName, groupPath, description, visibility})
}

async function fetchUserByHandle({handle}) {
  return GitLab.fetchUserByHandle({handle})
}

async function addMemberByIdToGroup({groupName, userId, accessLevel}) {
  return GitLab.addMemberByIdToGroup({groupName, userId, accessLevel})
}

let demo = {
    group: {
        name: "death-star"
      , path: "death-star"
      , description: "Death Star Project"
      , visibility: "public"
    }
  , users: ["k33g", "babs", "buster"]
}

// await is only valid in async function
async function batch(demo) {
  try {
    // Create the group
    let demoGroup = await createGroup({
        groupName: demo.group.name
      , groupPath: demo.group.path
      , description: demo.group.description
      , visibility: demo.group.visibility
    })

    // add users to the group
    for (const handle of demo.users) {
      let user = await fetchUserByHandle({handle: handle})
      console.log(user.id, user.name)
      await addMemberByIdToGroup({
          groupName: demo.group.name
        , userId: user.id
        , accessLevel: 50 // Owner access # Only valid for groups
      })
    }

  } catch (error) {
    console.log("😡:", error)
  }
}

batch(demo)



